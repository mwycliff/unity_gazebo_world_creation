##.  VALOR -- Build a World in Gazebo Using 3D Map Data from Unity





#.	Step Zero:  Gain Access to the Unity Simulation

  	Log on to the Windows 10 machine that hosts the VALOR demo

\#	Username:  VRacious     Password:  VRacious!





#.	Step One:  Open a Windows explorer Tab and navigate
	
	to the directory with the VALOR Appartment Unity launcher.

	Enter the following keystrokes: 'Windows' , 'R' , 'Enter'

	When the Run / Browse window appears, populate the field with:

	D:\Projects\Valor\VALOR_Beta\Assets\VALOR\Scenes

	-- and hit Enter.  Double click on Appartment_01.

	Unity should start.  A 3D apparment model should appear.





#.	Step Two:  Import the karl/pb_stl repository into the

	Packages directory in Windows. Clone or Download the

	pb_stl repository from https://github.com/Karl-/pb_stl

	and place it in the D:\Projects\Valor\VALOR_Beta\Packages\
	
	directory.  Restart Unity.





#.	Step Three:  Create a Game Object.

	To create a 'Game Object', mouse over the fourth

	tab from the left of the menu bar:

	|  File  |   Edit  |  Assets  |  Game Object  |

	Select the first line item that appears:

	'Create Empty', or type 'Ctrl + Shift + N'




#.	Step Four:  Populate the Game Object

	Mouse over the tab on the left hand

	side of the screen that says Virtual Environment.

	Click and drag it, or copy it onto the Game Object.

	The Virtual Environment block should now be nested

	under the 'Game Object' icon.




#.	Step Five:  Get Rid of Useless Items
	
	Mouse over the Virtual Environment and click

	the arrow next to it -- this will expand the

	tree structure, and make the names of

	all of the items in the Virtual Environment

	visible.  Get rid of all of the items that

	do not need to appear in the Gazebo Simulation.

	Those items may be:  Doors, door frames, paintings,

	windows, wall decor, a client alignment reference

	block, etc.




#.	Step Six:  Export the STL file

	Select the Game Object item on the left -- which

	should now contain only the items that you want

	to appear in the final Gazebo simulation.
	
	Click the 'Assets' tab at the top of the

	screen -- third from the left between 'Edit'

	and 'Game Object'.  The thirteenth option from the

	top should be 'Export Model'.  Two options should appear:

	STL ( Ascii )  and  STL ( Binary ).  Pick STL-Binary,

	and save it to a flash-memory-stick or a git-repository.
	


	
#.	Step Seven:  Power on your Linux Machine

	Open up a terminal and create a working directory for editing

	your newly created STL file.  From your home directory

	enter:		$ mkdir gazeboWorkspace
	
	      		$ cd    gazeboWorkspace
	
	Clone or copy your appartment.stl file from your flashstick

	or repository into your current directory, and then open it

	in meshlab.

	      		$ meshlab appartment.stl

	If you do not have meshlab, install it by typing:
	
	      		$ sudo apt-get update
	      		
	      		$ sudo apt-get install meshlab


#.	Step Eight:  Merge Close Vertices

	Mouse over the 'Filters' tab of the menu bar

	at the top of the screen.  Select 'Cleaning and Repairing'.

	Click 'Merge Close Vertices', then 

	type 'Ctrl + E' , ' <tab>  <tab>  <tab>  <enter> ' 
	

	
#.	Step Nine:  Decimate

	Open Blender and delete the big cube.

	If you do not have blender, install blender by typing:
	
	      		$ sudo apt-get update
	      		
	      		$ sudo apt-get install blender

	Import the appartment.stl by clicking File

	in the menu bar, followed by 'Import', followed

	by '.stl'

	Locate your file in the blender browser / explorer /

	navigator window, and then double click it.  The appartment

	should appear on the screen.

	Mouse over to the right side of the screen and locate the

	small wrench icon, which should be 2nd from the right.

	Click 'Add Modifier'.  A menu with four columns will appear.

	Select 'Decimate' which should be fifth item from the top of the

	'Generate' column -- which will be in the second column from the left.

	Select the 'Ratio' field, and enter '0.9' then hit enter.  Then

	hit copy.  Continue to click copy until you have reduced the number of

	polygons to a level that you are comfortable with.  Your aim

	should be to preserve the geometry of items that the firefighters

	must be aware of while getting rid of the unnecessary details.

	Once you have achieved a resolution that you feel comfortable with,

	click apply on all of the open 'Modifier' blocks.  They will

	disappear one by one.


#.	Step Ten: Open a fresh terminal

	Navigate to your previous working directory

	cd ~/gazeboWorkspace/

	Copy or clone the unityToGazebo directory into

	your current directory.

	
	$ git clone git@bitbucket.org:mwycliff/unity_gazebo_world_creation.git


	Enter the following commands:

	$ cd ./unityToGazebo/
	
	$ cp -r valor_appartment/ ~/.gazebo/models/


#.	Step Eleven:  Start Gazebo

	$ gazebo valor.world



##.	IF GAZEBO STARTS BUT YOU DO NOT SEE YOUR WORLD

PLEASE CONTACT MATTHEW WYCLIFF

	mwycliff@traclabs.com



##.	IF GAZEBO DOES NOT START

Make sure you have it installed.  Also make sure

the process is not already running.  You can find

out if it is already running by entering the following

command in the terminal:

$ sudo ps -A | grep gz



If the command returns 'gzclient' and / or 'gzserver'

then you must kill those processes before

you proceed.  To do so enter the following commands:

$ sudo pkill -SIGKILL gzclient

$ sudo pkill -SIGKILL gzserver